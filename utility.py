from random import randint, uniform
import matplotlib.pyplot as plt


def points_generate(file_name, points_quantity, lower, upper):
    f = open(file_name, 'w')
    points = []

    while len(points) != points_quantity:
        choice = [randint(lower, upper), randint(lower, upper)]
        if choice not in points:
            points.append(choice)

    for i in range(points_quantity):
        for x in points[i]:
            f.write("{}\t".format(x))
        f.write('\n')

    f.close()


def load_data(name):
    data = list()
    with open(name) as my_file:
        for line in my_file.read().split('\n'):
            tmp = []
            for value in line.split():
                tmp.append(float(value))
            data.append(tmp)
    data.remove([])
    return data


def plot(centroids, belongs):
    colors = list()
    while len(colors) != len(centroids):
        color = [uniform(0.05, 1), uniform(0, 1), uniform(0.1, 1)]
        if color not in colors:
            colors.append(color)

    colors = tuple(colors)

    for k_index, point in belongs:
        for index, k in enumerate(centroids):
            if k == centroids[k_index]:
                plt.scatter(point[0], point[1], s=25, color=colors[index], marker='o')

        plt.scatter(centroids[k_index][0], centroids[k_index][1], color='b', marker='x')
    plt.show()

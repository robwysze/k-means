"""
ZIAD Algorithm K-means Implementation [Centroids algorithm]
Robert Wyszecki
"""

from kmeans import kmeans
from utility import points_generate, load_data, plot
from os.path import abspath, exists


if __name__ == '__main__':
    centroid_numbers = 10
    points_number = 500
    lower = 0
    upper = 300

    file_name = str(points_number) + ' points.txt'
    my_file = abspath('data\\'+file_name)

    if not exists(my_file):
        points_generate(file_name, points_number, lower, upper)

    data_set = load_data(my_file)

    centroids, belong_to = kmeans(data_set, centroid_numbers, 0.000000001)
    plot(centroids, belong_to)

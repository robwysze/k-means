from random import randint
from math import pow, sqrt


def check_min(dataset):
    min_x = dataset[0][0]
    min_y = dataset[0][1]
    for point in dataset[1:]:
        if point[0] < min_x:
            min_x = point[0]
        if point[1] < min_y:
            min_y = point[1]
    return min_x, min_y


def check_max(dataset):
    max_x = dataset[0][0]
    max_y = dataset[0][1]
    for point in dataset[1:]:
        if point[0] > max_x:
            max_x = point[0]
        if point[1] > max_y:
            max_y = point[1]
    return max_x, max_y


def euclidean_distance(a, b):
    value = 0
    for i in range(len(a)):
        value += pow((a[i] - b[i]), 2)
    return sqrt(value)


def average(centroids, belong):

    for index, position in enumerate(centroids):
        counter = 0
        for k_index, belong_point in belong:
            if k_index == index:
                counter += 1
                for pos in range(len(position)):
                    centroids[index][pos] += belong_point[pos]

        if counter != 0:
            for pos in range(len(position)):
                centroids[index][pos] /= (counter+1)

    return centroids


def average_quantization_error(centroids, belong):
    dm = []
    for index, position in enumerate(centroids):
        counter = 0
        error = 0
        for k_index, belong_point in belong:
            if k_index == index:
                counter += 1
                error += quantization_error(position, belong_point)
        if counter != 0:
            dm.append(error/(counter+1))
        else:
            dm.append(error)
    return dm


def quantization_error(a, b):
    result = 0
    for i in range(len(a)):
        result += pow(a[i]-b[i], 2)
    return result


def kmeans(data, k, epsilon):
    centroids = list()
    centroids_old = list()
    centroids_history = list()
    done = False

    max_x, max_y = check_max(data)
    min_x, min_y = check_min(data)
    # centroid set
    while len(centroids) != k:
        point = [randint(min_x, max_x), randint(min_y, max_y)]
        if point not in centroids:
            centroids.append(point)
            centroids_old.append([0, 0])

    error = 999
    iteration = 0
    while not done:
        if error < epsilon:
            done = True
        print('Iteration: {}'.format(iteration))
        belong = []

        for point in data:
            distance = euclidean_distance(centroids[0], point)
            belong.append([0, point])
            for k_index, k_position in enumerate(centroids):
                tmp_distance = euclidean_distance(k_position, point)
                if distance >= tmp_distance:
                    belong.pop()
                    belong.append([k_index, point])
                    distance = tmp_distance

        d_previous = average_quantization_error(centroids, belong)
        average(centroids, belong)
        d_current = average_quantization_error(centroids, belong)
        d_total = []

        for i in range(len(d_current)):
            if d_current[i] == 0:
                d_total.append(0)
            else:
                d_total.append((d_previous[i] - d_current[i])/d_current[i])

        for value in d_total:
            error += value
        error /= k

        iteration += 1

    return centroids, belong
